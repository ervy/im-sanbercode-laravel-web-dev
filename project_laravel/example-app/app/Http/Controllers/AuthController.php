<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function kirim(request $request) {
        $firstname = $request["firstname"];
        $lastname = $request["lastname"];
        $gender = $request["gender"];
        $nasionality = $request["nasioality"];
        $languagespoken = $request["language-spoken"];
        $message = $request["message"];
        return view('welcome',[
            "firstname"=>$firstname,
            "lastname"=>$lastname,
            "gender"=>$gender,
            "nasionality"=>$nasionality,
            "language-spoken"=>$languagespoken,
            "message"=>$message
        ]); 
    }

}
