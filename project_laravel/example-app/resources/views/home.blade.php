  @extends('layout.master')
  @section('title')
    Halaman Utama
  @endsection
  @section('subtitle')
    Subtitle halaman utama
  @endsection
  @section('content')
    <h1>SanberBook</h1>
    <h2>Social Media Developer Santai Berkualitas</h2>
    <p>Belajar dan Berbagi agar hidup ini semakin santai berkualitas</p>
    <h3>Benefit Join di SanberBook</h3>
    <ul>
        <li>Mendapatkan motivasi dari semua developer</li>
        <li>Sharing knowledge dari para mastah Samber</li>
        <li>Dibuat oleh calon web developer</li>
    </ul>
    <h3>Cara Bergabung ke SanberBook</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
        <li>Selesai!</li>
    </ol>
  @endsection

  
