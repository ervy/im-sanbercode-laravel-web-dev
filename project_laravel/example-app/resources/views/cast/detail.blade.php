@extends('layout.master')
@section('title')
  Halaman Detail Cast
@endsection
@section('subtitle')
  Detail Cast
@endsection

@section('content')
<h1 class="text-primary mb-4">{{$cast->nama}}</h1>
<p>Umur : <br> {{$cast->umur}}</p>
<p>Bio : <br> {{$cast->bio}}</p>
@endsection