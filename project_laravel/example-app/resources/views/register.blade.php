@extends('layout.master')
@section('title')
  Halaman Registrasi
@endsection
@section('subtitle')
  Subtitle Halaman Registrasi
@endsection
@section('content')
<h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        
    @csrf

    <label>First name:</label><br><br>
        <input type="text" class="first-name" name="firstname"><br><br>
    <label>Last name:</label><br><br>
        <input type="text" class="last-name" name="lastname"><br><br>
    <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>
    <label>Nationality:</label><br><br>
        <select name="nasionality">
            <option value="bahasa-indonesia">Bahasa Indonesia</option>
            <option value="english">English</option>
            <option value="other">Other</option>
        </select><br><br>
    <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language-spoken" value="bahasa-indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language-spoken" value="english">English<br>
        <input type="checkbox" name="language-spoken" value="other">Other<br><br>
    <label>Bio:</label><br><br>
        <textarea name="message" cols="30" rows="10"></textarea><br><br>    
        <input type="submit" value="kirim">
    </form>
@endsection
    